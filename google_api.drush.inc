<?php

/**
 * @file
 *   Google API drush commands.
 *
 */

/**
 * Implementation of hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * Notice how this structure closely resembles how
 * you define menu hooks.
 *
 * See `drush topic docs-commands` for a list of recognized keys.
 *
 * @return
 *   An associative array describing your command(s).
 */
function google_api_drush_command() {
  $items = array();
  
  $items['google-api-get'] = array(
    'description' => "Downloads and extracts the Google APIs Client Library for PHP",
    'arguments' => array(
      'path' => dt('Optional. A path to the download folder. If omitted Drush will use the default location (sites/all/libraries/google-api-php-client).'),
    ),        
    'examples' => array(
      'drush gapi',
    ),
    'aliases' => array('gapi'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );
  
  return $items;
}

/**
 * Implementation of hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'. This hook is optional. If a command
 * does not implement this hook, the command's description is used instead.
 *
 * This hook is also used to look up help metadata, such as help
 * category title and summary.  See the comments below for a description.
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for your command.
 */
function google_api_drush_help($section) {
  switch ($section) {
    case 'drush:google-api-get':
      return dt("Download the Google APIs Client Library for PHP and put it into your libraries folder");    
    // The 'summary' meta item is displayed in `drush help --filter`,
    // and is used to give a general idea what the commands in this
    // command file do, and what they have in common.
    case 'meta:sandwich:summary':
      return dt("Download Google APIs PHP library");
  }
}

/**
 * A command callback.
 */
function drush_google_api_get() {
  $args = func_get_args();
  if (isset($args[0])) {
    $path = $args[0];
  }
  else {
    $path = drush_get_context('DRUSH_DRUPAL_ROOT');
    if (module_exists('libraries')) {
      $path .= '/' . libraries_get_path('google-api-php-client');
    }
    else {
      $path .= '/' . drupal_get_path('module', 'google_api') . '/google-api-php-client';
    }
  }

  if (is_dir($path)) {
    drush_log('Google APIs Client for PHP is already present. No download required.', 'ok');
  }
  elseif (drush_shell_exec('svn export http://google-api-php-client.googlecode.com/svn/trunk/ ' . $path)) {
    drush_log(dt('Google APIs Client for PHP  has been exported via svn to @path.', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to export Google APIs Client for PHP  to @path.', array('@path' => $path)), 'warning');
  }
}

/**
 * Implements drush_MODULE_post_COMMAND().
 */
function drush_google_api_post_pm_enable() {
  $extensions = func_get_args();
  // Deal with comma delimited extension list.
  if (strpos($extensions[0], ',') !== FALSE) {
    $extensions = explode(',', $extensions[0]);
  }

  if (in_array('google_api', $extensions) && !drush_get_option('skip')) {
    drush_google_api_get();
  }
}

